package fr.cnam.foad.nfa035.badges.gui;

import fr.cnam.foad.nfa035.badges.gui.components.BadgePanel;
import fr.cnam.foad.nfa035.badges.gui.model.BadgesModel;
import fr.cnam.foad.nfa035.badges.gui.renderer.BadgeSizeCellRenderer;
import fr.cnam.foad.nfa035.badges.gui.renderer.DefaultBadgeCellRenderer;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 * classe de définition de l'interface graphique du programme
 */
public class BadgeWalletGUI {
    private JButton button1;
    private JPanel panelBas;
    private JTable table1;
    private JScrollPane scrollBas;
    private JScrollPane scrollHaut;
    private JPanel panelParent;
    private JPanel panelHaut;
    private JPanel panelImageContainer;
    /**
     * aggrégation vers objet BadgePanel
     */
    BadgePanel badgePanel;
    /**
     * aggrégation vers objet DigitalBadge
     */
    DigitalBadge badge;
    /**
     * aggrégation vers objet DirectAccessBadgeWalletDAO
     */
    DirectAccessBadgeWalletDAO dao;
    /**
     * aggrégation vers List DigitalBadge
     */
    List<DigitalBadge> tableList; // remis en global

    private static final String RESOURCES_PATH = "exercice-2/badges-gui/src/main/resources/";

    public BadgeWalletGUI() {

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Bien joué!");
            }
        });

        //DirectAccessBadgeWalletDAO dao;

        // 1. Le Model
        //dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
        //Set<DigitalBadge> metaSet = dao.getWalletMetadata();
        //List<DigitalBadge> tableList = new ArrayList<>();
        //tableList.addAll(metaSet);
        //TableModel tableModel = TableModelCreator.createTableModel(DigitalBadge.class, tableList);
        BadgesModel tableModel = new BadgesModel(tableList);
        badgePanel.setPreferredSize(scrollHaut.getPreferredSize()); // adaptation à la taille de badgePanel

        // 2. Les Renderers...
        table1.setModel(tableModel);
        table1.getColumnModel().getColumn(4).setCellRenderer(new BadgeSizeCellRenderer());
        table1.setDefaultRenderer(Object.class, new DefaultBadgeCellRenderer());
        // Apparemment, les Objets Number sont traités à part, donc il faut le déclarer explicitement en plus de Object
        table1.setDefaultRenderer(Number.class, new DefaultBadgeCellRenderer());
        // Idem pour les Dates
        table1.setDefaultRenderer(Date.class, new DefaultBadgeCellRenderer());

        // 3. Tri initial
        table1.getRowSorter().toggleSortOrder(0);

        // définition click affichage image
        table1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                super.mouseClicked(event);
                if (event.getClickCount() == 2) {
                    int row = ((JTable) event.getComponent()).getSelectedRow();
                    panelHaut.removeAll();
                    panelHaut.revalidate();

                    badge = tableList.get(row);
                    setCreatedUIFields(); //appel de cette fonction

                    panelHaut.add(scrollHaut);
                    panelImageContainer.setPreferredSize(new Dimension(256, 256));
                    scrollHaut.setViewportView(panelImageContainer);

                    panelHaut.repaint();
                }
            }
        });

    }

    /**
     * Fonction main d'entrée du programme
     *
     * @param args les arguments
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame("My Badge Wallet");
        BadgeWalletGUI gui = new BadgeWalletGUI();
        frame.setContentPane(gui.panelParent); //englobe tout

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setIconImage(gui.getIcon().getImage());
        frame.setVisible(true);
    }

    /**
     * détermine le chemin pour l'icône
     *
     * @return ImageIcon
     */
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/logo.png"));
    }

    /**
     * création d'un composant personnalisé
     */
    private void createUIComponents() {
        try {
            this.dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
            Set<DigitalBadge> metaSet = new TreeSet<>(dao.getWalletMetadata()); // pour afficher image dans bon ordre
            this.tableList = new ArrayList<>();
            tableList.addAll(metaSet);
            this.badge = tableList.get(0); //affiche par défaut 1er badge

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        this.setCreatedUIFields();
        // TODO: place custom component creation code here
    }

    private void setCreatedUIFields() {
        this.badgePanel = new BadgePanel(dao, badge); //appel contructeur avec 2 param
        this.badgePanel.setPreferredSize(new Dimension(256, 256)); //dimension du badge
        this.panelImageContainer = new JPanel(); // instanciation de PanelImageContainer
        this.panelImageContainer.add(badgePanel); // ajout dynamique de l'image dans PanelImagecontainer
    }

}
