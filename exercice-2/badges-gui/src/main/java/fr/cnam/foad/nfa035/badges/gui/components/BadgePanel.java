package fr.cnam.foad.nfa035.badges.gui.components;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class BadgePanel extends JPanel {

    /**
     * aggrégation vers objet DigitalBadge
     */
    DigitalBadge badge;
    /**
     * aggrégation vers objet DirectAccessBadgeWalletDAO
     */
    DirectAccessBadgeWalletDAO dao;

    /**
     * constructeur avec 2 paramètres
     * @param dao
     * @param badge
     */
    public BadgePanel(DirectAccessBadgeWalletDAO dao, DigitalBadge badge) {
        this.dao = dao;
        this.badge = badge;
    }

    /**
     * affichaur de l'image
     * @param g the <code>Graphics</code> object to protect
     */
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            dao.getBadgeFromMetadata(bos, badge);
            g.drawImage(ImageIO.read(new ByteArrayInputStream(bos.toByteArray())), 0, 0, this.getWidth(), this.getHeight(), this);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    public DigitalBadge getBadge() {
        return badge;
    }

    public void setBadge(DigitalBadge badge) {
        this.badge = badge;
    }

    public DirectAccessBadgeWalletDAO getDao() {
        return dao;
    }

    public void setDao(DirectAccessBadgeWalletDAO dao) {
        this.dao = dao;
    }
}
